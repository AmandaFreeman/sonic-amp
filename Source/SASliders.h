/*
  ==============================================================================

    SASliders.h
    Created: 22 Dec 2020 8:08:39pm
    Author:  sonichertz

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>

class SAGainSlider : public juce::Slider
{
public:
	SAGainSlider();
	~SAGainSlider();
	
private:
};



class SADistortionSlider : public juce::Slider
{
public:
	SADistortionSlider();
	~SADistortionSlider();
	
private:
};

