/*
  ==============================================================================

    SASliders.cpp
    Created: 22 Dec 2020 8:08:39pm
    Author:  sonichertz

  ==============================================================================
*/

#include "SASliders.h"
SAGainSlider::SAGainSlider()
{
	setSliderStyle(juce::Slider::SliderStyle::LinearVertical);
	setTextBoxStyle(juce::Slider::NoTextBox, true, 0, 0);
	setRange(-48.0, 0.0);
}

SAGainSlider::~SAGainSlider()
{
}

SADistortionSlider::SADistortionSlider()
{
	setSliderStyle(juce::Slider::SliderStyle::LinearVertical);
	setTextBoxStyle(juce::Slider::NoTextBox, true, 0, 0);
	setRange(0.0f, 1.0f);
}

SADistortionSlider::~SADistortionSlider()
{
	
}
