/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"

//==============================================================================
SonicAmpAudioProcessorEditor::SonicAmpAudioProcessorEditor (SonicAmpAudioProcessor& p)
    : AudioProcessorEditor (&p), audioProcessor (p)
{
    // Make sure that before the constructor has finished, you've set the
    // editor's size to whatever you need it to be.
    setSize (400, 300);
	
	//Set up sliders
	/* gain slider */
	att_slider_gain = std::make_unique<juce::AudioProcessorValueTreeState::SliderAttachment> (audioProcessor.apvts, "InputGain", slider_gain);
	slider_gain.addListener(this);
	addAndMakeVisible(slider_gain);
	
	/* Distortion slider NOTE THERE'S NO PROCESSOR CODE YET; JUST THE SLIDER */
	att_slider_distortion = std::make_unique<juce::AudioProcessorValueTreeState::SliderAttachment> (audioProcessor.apvts, "Distortion", slider_distortion);
	slider_distortion.addListener(this);
	addAndMakeVisible(slider_distortion);

}

SonicAmpAudioProcessorEditor::~SonicAmpAudioProcessorEditor()
{
	att_slider_gain.reset();
	att_slider_distortion.reset();
}

//==============================================================================
void SonicAmpAudioProcessorEditor::paint (juce::Graphics& g)
{
    // (Our component is opaque, so we must completely fill the background with a solid colour)
    g.fillAll (getLookAndFeel().findColour (juce::ResizableWindow::backgroundColourId));

    g.setColour (juce::Colours::white);
//    g.setFont (15.0f);
//    g.drawFittedText ("Hello World!", getLocalBounds(), juce::Justification::centred, 1);
	
//	g.setColour (juce::Colours::red);
//	g.drawRect(distortion_slider_area);
}

void SonicAmpAudioProcessorEditor::resized()
{
	/* REFACTOR THIS */
	juce::Rectangle<int> whole_area;
	juce::Rectangle<int> gain_slider_area;
	juce::Rectangle<int> distortion_slider_area;
	whole_area.setBounds(0, 0, getWidth(), getHeight());
						  
	gain_slider_area = whole_area.removeFromLeft(getWidth()/2);
	distortion_slider_area = whole_area;
	
	slider_gain.setBounds(gain_slider_area);
	slider_distortion.setBounds(distortion_slider_area);
}

void SonicAmpAudioProcessorEditor::sliderValueChanged(juce::Slider *slider) { 
	if (slider == &slider_gain)
	{
//		audioProcessor.mGain = pow (10, slider_gain.getValue() / 20);
		audioProcessor.mInputGain = juce::Decibels::decibelsToGain(slider_gain.getValue());
	}
	
	if (slider == &slider_distortion)
	{
		audioProcessor.mDistortion = slider_distortion.getValue();
	}
}

