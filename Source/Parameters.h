/*
  ==============================================================================

    Parameters.h
    Created: 31 Oct 2020 2:38:41pm
    Author:  sonichertz

  ==============================================================================
*/

#pragma once

// you don't have to give enumerated lists a name
enum SonicAmpParameter
{
	saParameter_InputGain = 0,
	saParameter_Distortion,

	saParameter_TotalNumParameters, // you can use this in a forloop to iterate over parameters
};

static float SAParameterDefaultValue [ saParameter_TotalNumParameters ] =
{
	-6.0, // input gain
	 0.0, // distortion
};

static float SAParameterRange [ saParameter_TotalNumParameters ][2] =
{
	{-48.0f, 0.0f}, // input gain
	{0.0f, 1.0f},   // distortion
};

static juce::String SAParameterID [ saParameter_TotalNumParameters ] =
{
	"InputGain",
	"Distortion",
};

static juce::String SAParameterLabel [ saParameter_TotalNumParameters ] =
{
	"Input Gain",
	"Distortion",
};

