/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"
#include "Parameters.h"

//==============================================================================
SonicAmpAudioProcessor::SonicAmpAudioProcessor()
#ifndef JucePlugin_PreferredChannelConfigurations
     : AudioProcessor (BusesProperties()
                     #if ! JucePlugin_IsMidiEffect
                      #if ! JucePlugin_IsSynth
                       .withInput  ("Input",  juce::AudioChannelSet::stereo(), true)
                      #endif
                       .withOutput ("Output", juce::AudioChannelSet::stereo(), true)
                     #endif
                       ),
#endif
// commented out mInputGain init as it overwrites retrieved xml data. You might need to initialise, could contain garbage on first load
//mInputGain(0.2f),
apvts(*this, nullptr, juce::Identifier("SonicAmpIdentifier"), createParameterLayout())
{
	mPresetManager = std::make_unique<SAPresetManager>(this);
}

SonicAmpAudioProcessor::~SonicAmpAudioProcessor()
{
}

//==============================================================================
const juce::String SonicAmpAudioProcessor::getName() const
{
    return JucePlugin_Name;
}

bool SonicAmpAudioProcessor::acceptsMidi() const
{
   #if JucePlugin_WantsMidiInput
    return true;
   #else
    return false;
   #endif
}

bool SonicAmpAudioProcessor::producesMidi() const
{
   #if JucePlugin_ProducesMidiOutput
    return true;
   #else
    return false;
   #endif
}

bool SonicAmpAudioProcessor::isMidiEffect() const
{
   #if JucePlugin_IsMidiEffect
    return true;
   #else
    return false;
   #endif
}

double SonicAmpAudioProcessor::getTailLengthSeconds() const
{
    return 0.0;
}

int SonicAmpAudioProcessor::getNumPrograms()
{
    return 1;   // NB: some hosts don't cope very well if you tell them there are 0 programs,
                // so this should be at least 1, even if you're not really implementing programs.
}

int SonicAmpAudioProcessor::getCurrentProgram()
{
    return 0;
}

void SonicAmpAudioProcessor::setCurrentProgram (int index)
{
}

const juce::String SonicAmpAudioProcessor::getProgramName (int index)
{
    return {};
}

void SonicAmpAudioProcessor::changeProgramName (int index, const juce::String& newName)
{
}

//==============================================================================
void SonicAmpAudioProcessor::prepareToPlay (double sampleRate, int samplesPerBlock)
{
	/* InputGain value will be default, saved or current slider value **/
	mPreviousGain = *apvts.getRawParameterValue("InputGain");
	mPreviousDistortion = *apvts.getRawParameterValue("Distortion");
}

void SonicAmpAudioProcessor::releaseResources()
{
    // When playback stops, you can use this as an opportunity to free up any
    // spare memory, etc.
}

#ifndef JucePlugin_PreferredChannelConfigurations
bool SonicAmpAudioProcessor::isBusesLayoutSupported (const BusesLayout& layouts) const
{
  #if JucePlugin_IsMidiEffect
    juce::ignoreUnused (layouts);
    return true;
  #else
    // This is the place where you check if the layout is supported.
    // In this template code we only support mono or stereo.
    if (layouts.getMainOutputChannelSet() != juce::AudioChannelSet::mono()
     && layouts.getMainOutputChannelSet() != juce::AudioChannelSet::stereo())
        return false;

    // This checks if the input layout matches the output layout
   #if ! JucePlugin_IsSynth
    if (layouts.getMainOutputChannelSet() != layouts.getMainInputChannelSet())
        return false;
   #endif

    return true;
  #endif
}
#endif

void SonicAmpAudioProcessor::processBlock (juce::AudioBuffer<float>& buffer, juce::MidiBuffer& midiMessages)
{
    juce::ScopedNoDenormals noDenormals;
    auto totalNumInputChannels  = getTotalNumInputChannels();
    auto totalNumOutputChannels = getTotalNumOutputChannels();

    // In case we have more outputs than inputs, this code clears any output
    // channels that didn't contain input data, (because these aren't
    // guaranteed to be empty - they may contain garbage).
    // This is here to avoid people getting screaming feedback
    // when they first compile a plugin, but obviously you don't need to keep
    // this code if your algorithm always overwrites all the output channels.
    for (auto i = totalNumInputChannels; i < totalNumOutputChannels; ++i)
        buffer.clear (i, 0, buffer.getNumSamples());

	
	/* smooth gain changes with a ramp */
	// NOTE: raw InputGain value will be either default, saved or whatever you set the slider to
	float currentGain = juce::Decibels::decibelsToGain((float) *apvts.getRawParameterValue("InputGain"));

	if (mPreviousGain == currentGain)
	{
		buffer.applyGain(currentGain);
	} else {
		buffer.applyGainRamp(0, buffer.getNumSamples(), mPreviousGain, currentGain);
		mPreviousGain = currentGain;
	}
	/*----------------------------------**/
	
    // This is the place where you'd normally do the guts of your plugin's
    // audio processing...
    // Make sure to reset the state if your inner loop is processing
    // the samples and the outer loop is handling the channels.
    // Alternatively, you can process the samples with the channels
    // interleaved by keeping the same state.
//    for (int channel = 0; channel < totalNumInputChannels; ++channel)
//    {
//        auto* channelData = buffer.getWritePointer (channel);
//
//		for (int sample = 0; sample < buffer.getNumSamples(); ++sample)
//		{
//			channelData[sample] = buffer.getSample(channel, sample) * mInputGain;
//		}
//    }
	float mDistortion = *apvts.getRawParameterValue("Distortion");
}

//==============================================================================
bool SonicAmpAudioProcessor::hasEditor() const
{
    return true; // (change this to false if you choose to not supply an editor)
}

juce::AudioProcessorEditor* SonicAmpAudioProcessor::createEditor()
{
    return new SonicAmpAudioProcessorEditor (*this);
}

//==============================================================================
void SonicAmpAudioProcessor::getStateInformation (juce::MemoryBlock& destData)
{
	juce::XmlElement preset("SA_StateInfo");
	juce::XmlElement* presetBody = new juce::XmlElement("SA_Preset");
	
	mPresetManager->getXmlForPreset(presetBody);
	preset.addChildElement(presetBody);
	copyXmlToBinary(preset, destData);
}

void SonicAmpAudioProcessor::setStateInformation (const void* data, int sizeInBytes)
{
	std::unique_ptr<juce::XmlElement> xmlState = (getXmlFromBinary(data, sizeInBytes));

	if(xmlState != nullptr){
		// juce macro that auto iterates over the xml element to access all child attributes as subchild
		forEachXmlChildElement(*xmlState, subChild){
			mPresetManager->loadPresetForXml(subChild);
		}
	} else {
		jassertfalse;
	}
}

juce::AudioProcessorValueTreeState::ParameterLayout SonicAmpAudioProcessor::createParameterLayout() { 
	std::vector<std::unique_ptr<juce::AudioParameterFloat>> params;
	
	for(int i = 0; i < saParameter_TotalNumParameters; i++){
	params.push_back(std::make_unique<juce::AudioParameterFloat>(SAParameterID[i],
									  SAParameterLabel[i],
									  juce::NormalisableRange<float> (SAParameterRange[i][0], SAParameterRange[i][1]),
									  SAParameterDefaultValue[i]));
	}

	return { params.begin(), params.end() };
}




//==============================================================================
// This creates new instances of the plugin..
juce::AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    return new SonicAmpAudioProcessor();
}
