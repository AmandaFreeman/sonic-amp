/*
  ==============================================================================

    This file contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#pragma once

#include <JuceHeader.h>
#include "PluginProcessor.h"
#include "SASliders.h"

//==============================================================================
/**
*/
class SonicAmpAudioProcessorEditor  : public juce::AudioProcessorEditor,
public juce::Slider::Listener
{
public:
    SonicAmpAudioProcessorEditor (SonicAmpAudioProcessor&);
    ~SonicAmpAudioProcessorEditor() override;

    //==============================================================================
    void paint (juce::Graphics&) override;
    void resized() override;
	
	std::unique_ptr<juce::AudioProcessorValueTreeState::SliderAttachment> att_slider_gain;
	std::unique_ptr<juce::AudioProcessorValueTreeState::SliderAttachment> att_slider_distortion;
private:
    // This reference is provided as a quick way for your editor to
    // access the processor object that created it.
    SonicAmpAudioProcessor& audioProcessor;
	
//	juce::Slider slider_gain;
	SAGainSlider slider_gain;
	SADistortionSlider slider_distortion;
	
	void sliderValueChanged (juce::Slider *slider) override;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (SonicAmpAudioProcessorEditor)
};
