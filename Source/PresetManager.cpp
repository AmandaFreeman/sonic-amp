/*
  ==============================================================================

    PresetManager.cpp
    Created: 31 Oct 2020 6:08:12pm
    Author:  sonichertz

  ==============================================================================
*/

#include "PresetManager.h"

#if JUCE_WINDOWS
	static const juce::String directorySeperator = "\\";
#elif JUCE_MAC
	static const juce::String directorySeperator = "/";
#endif


SAPresetManager::SAPresetManager(juce::AudioProcessor* inProcessor) :
	mCurrentPresetIsSaved(false),
	mCurrentPresetName("Untitled"),
	mProcessor(inProcessor)
{
	// store the name of the plugin so you can use it for any plugin you write
	const juce::String pluginName = (juce::String) mProcessor->getName();
	
	mPresetDirectory =
	(juce::File::getSpecialLocation(juce::File::userDesktopDirectory)).getFullPathName() + directorySeperator + pluginName;
	
	if(!juce::File(mPresetDirectory).exists()){
		juce::File(mPresetDirectory).createDirectory();
	}
	
	storeLocalPreset();
}

SAPresetManager::~SAPresetManager()
{
	
}

void SAPresetManager::getXmlForPreset(juce::XmlElement* inElement)
{
	auto& parameters = mProcessor->getParameters();
	
	for(int i = 0; i < parameters.size(); i++){
		juce::AudioProcessorParameterWithID* parameter =
		(juce::AudioProcessorParameterWithID*)parameters.getUnchecked(i);
		
		inElement->setAttribute(parameter->paramID, parameter->getValue());
	}
}

void SAPresetManager::loadPresetForXml(juce::XmlElement* inElement)
{
	mCurrentPresetXml = inElement;

//	iterate over xml for attribute name and value
	auto& parameters = mProcessor->getParameters();
	
	for(int i = 0; i < mCurrentPresetXml->getNumAttributes(); i++){
		const juce::String paramId = mCurrentPresetXml->getAttributeName(i);
		const float value = mCurrentPresetXml->getDoubleAttribute(paramId);
		
		for(int j = 0; j < parameters.size(); j++){
			juce::AudioProcessorParameterWithID* parameter =
			(juce::AudioProcessorParameterWithID*)parameters.getUnchecked(i);
			
			if(paramId == parameter->paramID){
				parameter->setValueNotifyingHost(value);
			}
		}
	}
}

int SAPresetManager::getNumberOfPresets()
{
	return mLocalPresets.size();
}
	
juce::String SAPresetManager::getPresetName(int inPresetIndex)
{
	return mLocalPresets[inPresetIndex].getFileName();
}
	
void SAPresetManager::createNewPreset()
{
	auto& parameters = mProcessor->getParameters();
	
	for(int i = 0; i < parameters.size(); i++){
		juce::AudioProcessorParameterWithID* parameter =
		(juce::AudioProcessorParameterWithID*)parameters.getUnchecked(i);
		
		const float defaultValue = parameter->getDefaultValue();
		
		parameter->setValueNotifyingHost(defaultValue);
	}
	
	mCurrentPresetIsSaved = false;
	mCurrentPresetName = "Untitled";
}

void SAPresetManager::savePreset()
{
	juce::MemoryBlock destinationData;
	mProcessor->getStateInformation(destinationData); // get state info from processor and store in memory block
	mCurrentlyLoadedPreset.deleteFile();              // delete currently loaded preset
	mCurrentlyLoadedPreset.appendData(destinationData.getData(), destinationData.getSize()); //append new data to it to recreate it
	mCurrentPresetIsSaved = true;
}

void SAPresetManager::saveAsPreset(juce::String inPresetName)
{
	juce::File presetFile = juce::File(mPresetDirectory + directorySeperator + inPresetName + PRESET_FILE_EXTENSION);
	
	if(!presetFile.exists()){
		presetFile.create();
	} else {
		presetFile.deleteFile();
	}
	
	juce::MemoryBlock destinationData;
	mProcessor->getStateInformation(destinationData);
	
	presetFile.appendData(destinationData.getData(), destinationData.getSize());
	
	mCurrentPresetIsSaved = true;
	mCurrentPresetName = inPresetName;
	
	storeLocalPreset();
}

void SAPresetManager::loadPreset(int inPresetIndex)
{
	mCurrentlyLoadedPreset = mLocalPresets[inPresetIndex];
	
	juce::MemoryBlock presetBinary;
	
	if(mCurrentlyLoadedPreset.loadFileAsData(presetBinary)){
		mCurrentPresetIsSaved = true;
		mCurrentPresetName = getPresetName(inPresetIndex);
		mProcessor->setStateInformation(presetBinary.getData(), (int)presetBinary.getSize());
	}
}

bool SAPresetManager::getIsCurrentPresetSaved()
{
	return mCurrentPresetIsSaved;
}

juce::String SAPresetManager::getCurrentPresetName()
{
	return mCurrentPresetName;
}

void SAPresetManager::storeLocalPreset()
{
	mLocalPresets.clear();
	
	// iterates through the directory returning each file found. DIRECTORY ITERATOR IS DEPRECATED.
	// constructor args for: directory, is Recursive, what to find, what type of files
//	for(juce::DirectoryIterator di (juce::File(mPresetDirectory), false, "*"+(juce::String)PRESET_FILE_EXTENSION,
//							  juce::File::TypesOfFileToFind::findFiles); di.next();)
//	{
//		juce::File preset = di.getFile();
//		mLocalPresets.add(preset);
//	}
	
	for (juce::DirectoryEntry entry : juce::RangedDirectoryIterator (juce::File (mPresetDirectory), false, "*"+(juce::String)PRESET_FILE_EXTENSION, juce::File::TypesOfFileToFind::findFiles))
	{
		juce::File preset = entry.getFile();
		mLocalPresets.add(preset);
	}
	
	
}

