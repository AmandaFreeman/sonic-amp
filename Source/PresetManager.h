/*
  ==============================================================================

    PresetManager.h
    Created: 31 Oct 2020 6:08:12pm
    Author:  sonichertz

  ==============================================================================
*/

#pragma once

#include "JuceHeader.h"

// using .sap for Sonic Amp Preset file. You can use whatever you want here
#define PRESET_FILE_EXTENSION ".sap"

class SAPresetManager
{
public:
	SAPresetManager(juce::AudioProcessor* inProcessor);
	~SAPresetManager();

	void getXmlForPreset(juce::XmlElement* inElement);
	
	void loadPresetForXml(juce::XmlElement* inElement);
	
	int getNumberOfPresets();
	
	juce::String getPresetName(int inPresetIndex);
	
	void createNewPreset();
	
	void savePreset();
	
	void saveAsPreset(juce::String inPresetName);
	
	void loadPreset(int inPresetIndex);
	
	bool getIsCurrentPresetSaved(); // determine if need save or save as
	
	juce::String getCurrentPresetName();
	
private:
	// private as it's only for preset manager to call
	void storeLocalPreset();
	
	bool mCurrentPresetIsSaved;
	
	juce::File mCurrentlyLoadedPreset;
	
	juce::Array<juce::File> mLocalPresets;
	
	juce::String mCurrentPresetName;
	
	juce::String mPresetDirectory;
	
	juce::XmlElement* mCurrentPresetXml;
	juce::AudioProcessor* mProcessor;
};
